import mongoose from "mongoose";
const viewSchema = new mongoose.Schema({
    productId: { type: mongoose.Schema.Types.ObjectId, ref: 'Product' },
    quantity: {type: Number , default: 1},
    createdAt: { type: Number, default: Date.now },
});

const ViewModel = mongoose.model('view', viewSchema);
export default ViewModel 